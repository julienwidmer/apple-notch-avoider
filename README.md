# Apple Notch avoider

If users browse your website on a new iPhone in landscape mode, there are chances that your content might be hidden behind the notch.

With the use of CSS3 Media Queries, it is possible to target those specific devices in landscape and to add bigger margins or padding for your content.

## Device Screen Sizes Sources

[Adaptivity and Layout - Visual Design - iOS - Human Interface Guidelines - Apple Developer](https://developer.apple.com/design/human-interface-guidelines/ios/visual-design/adaptivity-and-layout/)


# Author
Hi! I’m [Julien Widmer](https://www.julienwidmer.ca) - Feel free to send me an [Email](mailto:hello@julienwidmer.ca) or to follow me on [Twitter](http://twitter.com/Qasph).